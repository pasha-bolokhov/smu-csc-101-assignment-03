# Assignment 3

All solutions must be placed on [CodeAnywhere](https://codeanywhere.com) into the folder `a03`.
Put each problem into the corresponding file `p1.py`, `p2.py`, `p3.py` *etc*.

All files must produce *no errors* when invoked with Python *e.g.* as
```
python p1.py
```




### Problem 1 (*20%*)

Write a python function to add up the first `n` odd integers.  Note that the `i`-th odd integer is given by the expression `2 * i - 1`

Put your solution into file `p1.py`




### Problem 2 (*40%*)

The mathematical exponent function can be calculated as an infinite series

    exp(x)  =  sum(n goes from 0 to "infinity") of x**n / n!

Consult item (2) in the [Characterizations of the exponential function](https://en.wikipedia.org/wiki/Characterizations_of_the_exponential_function) article if this notation is not clear

Write a function `exponent(x, num_terms)` which would calculate the exponent of `x` as a series of the above sum of only `num_terms` terms included.
That is, in the above sum `n` varies from `0` to `num_terms - 1`

Put your solution into file `p2.py`




### Problem 3 (*80%*)

Complete the code below to find the **second largest** integer number in the list.
The value should be assigned to variable `second_largest`.
Although the list is provided, your code should work for any list of integers.
Note that the second largest will be equal to the first largest only if the list
consists of all equal numbers (say `[1, 1, 1, 1, 1, 1]`)

Your complete code should look like below. Put your complete solution into file `p3.py`
```python
numbers = [30, -5, 102, -47, 19, 51, 7, 1186, -82, 72]

.... (place your code here) ....

print("second largest number =", second_largest)
```
